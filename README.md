# picturavis.com

This repository bundles the code, used plugins and settings for the [Piwigo](https://piwigo.com)-based photography website [picturavis.com](https://picturavis.com).


## Homepage
The landing page is set using the [Additional Pages](https://piwigo.org/ext/extension_view.php?eid=153)-plugin. The other connected static pages (except the Piwigo-gallery itself) are also made availble using this plugin.
The plugin unfortunately does not allow syncing via git, which is why the code has to be manually copied over from this repository. CSS also has to be set for each page separately.
The images for the pages are stored in a hidden album in Piwigo itself.

Due to the limits of the [Additional Pages](https://piwigo.org/ext/extension_view.php?eid=153)-plugin described above, the website needs to be rendered
into static HTML pages. This is described in the section below.

## Render Pages
To render the pages of the website, a Python environment with Jinja is required.
A new virtual environment can be created via
```shell
python3 -m venv venv
```
Activation of the environment depends on the shell. For the Fish shell it is
```shell
source venv/bin/activate.fish
```
Install the requirements via
```shell
pip install -r requirements.txt
```
Rendering of the pages is done by running
```
python render_pages.py
```
The pages of the website are now availble in the `pages` directory.

## Piwigo Plugins
The following plugins are used:
- [Additional Pages](https://piwigo.org/ext/extension_view.php?eid=153) (add static pages, like a landing page)
- [Advanced Menu Manager](https://piwigo.org/ext/extension_view.php?eid=250) (change which menus are shown in the gallery)
- [Copyrights](https://piwigo.org/ext/extension_view.php?eid=537) (set copyright information for each photo)
- [Exif View](https://piwigo.org/ext/extension_view.php?eid=155) (display EXIF information for each photo)
- [Exiftool Keywords](https://piwigo.org/ext/extension_view.php?eid=848) (read keywords from EXIF metadata)
- [Grum Plugin Classes](https://piwigo.org/ext/extension_view.php?eid=199) (dependency for other plugins)
- [GThumb+](https://piwigo.org/ext/extension_view.php?eid=591) (better thumbnail gallery)
- [Language Switch](https://piwigo.org/ext/extension_view.php?eid=123) (allow user to switch gallery language)
- [LocalFiles Editor](https://piwigo.org/ext/extension_view.php?eid=144) (edit files from the admin page)
- [Personal Favicon](https://piwigo.org/ext/extension_view.php?eid=462) (allow custom favicon)
- [PWG Stuffs](https://piwigo.org/ext/extension_view.php?eid=190) (add gallery modules, like random photos)
- [SmartAlbums](https://piwigo.org/ext/extension_view.php?eid=544) (allow automatic assignment of photos to albums)

Unfortunately for the managed hosting at [piwigo.com](https://piwigo.com), the Piwigo-team put a lot of plugins behind a steep paywall in March 2023 (see [their blog-post](https://piwigo.com/blog/2023/03/14/new-pricing-piwigo-com-what-does-it-change/), the [available plans](https://piwigo.com/pricing) and [plugins](https://rightful-baritone-cfe.notion.site/List-of-plugins-and-features-by-plan-cb81ecd2a398442d9e5056cd2a380588)).
If you are not already their customer (on a cheaper "legacy" plan), you either have to [self-host](https://piwigo.org/get-piwigo) a Piwigo instance or choose a different hosting-provider if you want to have plugins like [SmartAlbums](https://piwigo.org/ext/extension_view.php?eid=544) without paying a fortune.

## Piwigo Theme
The gallery uses the [Bootstrap Darkroom](https://piwigo.org/ext/extension_view.php?eid=831) theme with the following settings.

### Appearance
#### Bootstrap theme
- Color theme: `Bootstrap Darkroom`

#### Full width layout
- [x] Enable
- [x] Use 6 colums for viewports >= 1680px

#### Site logo
- [x] Enabled
- Path: URL to logo image in hidden album

#### Page header
- Banner style: `Hero image`
- Background image: URL to banner image
- [x] Integrate lower navbar
- [ ] Span the full viewport height

#### Category page display
- Display categories as Bootstrap media wells: `Never`
- [x] Display category description in grid view
- [x] Display number of images in album and subalbums 

#### Thumbnail page display
- [ ] Show image caption
- [ ] Use description rather than title for images
- Link thumbnail to: `Photoswipe Slideshow (Mobile devices only)`
- Description display style: `Advanced`

#### Picture page display
- Picture info display position: `Card grid below the image`

#### Custom CSS
```CSS
.page-header.page-header-small {
    height: 150px;
}

.page-header .page-header-image {
    z-index: 0;
}

.navbar.navbar-transparent {
    padding: 0px;
}

.copyright {
    padding: 0px;
}
```

### Components
#### Slick Carousel Settings
- [x] Enabled
- LazyLoad method: `ondemand`
- [x] Infinite looping
- [x] Center mode

#### PhotoSwipe Settings
- [X] Enabled
- [x] Autoplay interval: `3500` milliseconds

#### Quick search
- [x] Quick search directly in the navigation bar

#### Comments
- [x] Piwigo
- [ ] Disqus

#### Tag cloud
- [ ] Basic
- [x] HTML 5 canvas

