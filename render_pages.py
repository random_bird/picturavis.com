import sys
import shutil
import logging
from datetime import datetime
from pathlib import Path
from jinja2 import Environment, FileSystemLoader

logging.basicConfig(level=logging.DEBUG)
logger = logging.Logger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))

if __name__ == "__main__":
    # set paths
    pages_path = Path("pages")
    templates_path = Path("templates")

    # prepare target path for html pages
    if pages_path.exists():
        logger.info("Removing existing dir '%s'", pages_path)
        shutil.rmtree(pages_path)
    pages_path.mkdir(parents=True)

    # define varibales
    year = datetime.now().year

    # define jinja environment
    env = Environment(loader=FileSystemLoader(templates_path))

    # fill templates and store to pages dir
    for template_file in templates_path.glob("*"):
        if "html" in template_file.suffix:
            target_file = pages_path / template_file.name
            logger.info("Rendering '%s' to '%s'", template_file, target_file)
            env.get_template(template_file.name).stream(year=datetime.now().year).dump(
                str(target_file)
            )
